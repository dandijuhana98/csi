@extends('layouts.app')
@section('content')

    <div class="container">
        <div class=" form-row">
            <div class="col-lg-12">
                <h3>Edit Data Pertanyaan</h3>
            </div>
        </div>
        <br>

        @if ($errors->all())
            <div class="alert alert-danger">
                <strong>Whoops! </strong> Ada permasalahan inputanmu.<br>
                <ul>
                    @foreach ($errors as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="{{route('pertanyaan.update',$pertanyaan->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="kodetpk" class="form-control" value="{{$pertanyaan->kodetpk}}" placeholder="">
            <div class="form-group row">
                <label for="pertanyaan" class="col-sm-2 col-form-label">Pertanyaan</label>
                <div class="col-sm-10">
                    <input type="text" name="pertanyaan"  class="form-control" id="pertanyaan" value="{{$pertanyaan->pertanyaan}}" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label for="status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select id="status" name="status"class="form-control">
                        <option  {{$pertanyaan->status == 'Aktif' ? 'Selected' : ''}} value="Aktif">Aktif</option>
                        <option  {{$pertanyaan->status == 'Non Aktif' ? 'Selected' : ''}} value="Non Aktif">Non Aktif</option>
                    </select>
                </div>
            </div>
             <hr>
                <div class="form-group">
                    <a href="{{route('pertanyaan.index')}}" class="btn btn-success">Kembali</a>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
        </form>

    </div>
@endsection