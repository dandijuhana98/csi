@extends('layouts.app')
@section('content')
 
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Daftar Pertanyaan {{$tpk->namatpk}}</h3>
            </div>
        </div> 
        <br>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>        
        </div>
    @endif

    <div class="form-row">
        <div class="form-group mb-2 mr-auto">
            <a class="btn btn-success" href="{{ route('pertanyaan.create')}}"> Tambah Data </a>
        </div>
        <div class="form-group mb-2 ml-auto">
            <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
        </div>
    </div>
    
    <table class="table table-striped" id="table" data-toggle="table">
      <thead>
        <tr>
            <th data-order='desc' data-sortable="true">No</th>
            <th data-order='desc' data-sortable="true">Pertanyaan</th>
            <th data-order='desc' data-sortable="true">Status</th>
            <th data-order='desc' data-sortable="true">Action</th>
        </tr>
      </thead>
      <tbody id="myTable">
        @foreach ($pertanyaan as $p) 
            <tr>
                <td>{{++$i}}</td>
                <td>{{$p->pertanyaan}}</td>
                <td>{{$p->status}}</td>
                <td>
                    <form action="{{ route('pertanyaan.destroy',$p->id) }}" method="post">
                    <a class="btn btn-sm btn-success" href="{{ route('pertanyaan.show', $p->id)}}">Show</a>
                    <a class="btn btn-sm btn-warning" href="{{ route('pertanyaan.edit', $p->id)}}">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>    
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <br>
    Halaman : {{ $pertanyaan->currentPage() }} <br/>
	Jumlah Data : {{ $pertanyaan->total() }} <br/>
	Data Per Halaman : {{ $pertanyaan->perPage() }} <br/>

    {!! $pertanyaan->links() !!}
    </div>
    <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
    </script>

@endsection