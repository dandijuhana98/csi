@extends('layouts.app')
@section('content')

    <div class="container">
        <div class=" form-row">
            <div class="col-lg-12">
                <h3>Tambah Data Pertanyaan</h3>
            </div>
        </div>
        <br>

        @if ($errors->all())
            <div class="alert alert-danger">
                <strong>Whoops! </strong> Ada permasalahan inputanmu.<br>
                <ul>
                    @foreach ($errors as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="{{route('pertanyaan.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            
            <div class="form-group row">
                <div class="col-sm-10">
                    <input type="hidden" value="{{ Auth::user()->kodetpk }}" name="kodetpk" class="form-control" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="pertanyaan" class="col-sm-2 col-form-label">Pertanyaan</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="pertanyaan" rows="2" cols="80" required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select id="status" name="status"class="form-control" required>
                        <option value="Aktif">Aktif</option>
                        <option value="Non Aktif">Non Aktif</option>
                    </select>
                </div>
            </div>
             <hr>
                <div class="form-group">
                    <a href="{{route('pertanyaan.index')}}" class="btn btn-success">Kembali</a>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
        </form>

    </div>
    
@endsection