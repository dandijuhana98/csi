@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<div class="container">
        <div class=" form-row">
            <div class="col-lg-12">
                <h3 class="text-center">Tambah User</h3>
            </div>
        </div>
        <br>

        @if ($errors->all())
            <div class="alert alert-danger">
                <strong>Whoops! </strong> Ada permasalahan inputanmu.<br>
                <ul>
                    @foreach ($errors as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form method="POST" action="/user/store">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-5">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-5">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Area</label>
                            <div class="col-sm-5">
                                <select name="kodearea"class="form-control" required>
                                @foreach($area as $area)
                                    <option value="{{ $area->id }}">{{ $area->namaarea}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">TPK</label>
                            <div class="col-sm-5">
                                <select name="kodetpk"class="form-control" required>
                                    <option value="">-Pilih-</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Role</label>
                            <div class="col-sm-5">
                                <select name="role"class="form-control" required>
                                    <option value=""></option>
                                    <option value="ADMIN">ADMIN</option>
                                    <option value="ADMIN PUSAT">ADMIN PUSAT</option>
                                    <option value="ADMIN AREA">ADMIN AREA</option>
                                    <option value="ADMIN TPK">ADMIN TPK</option>
                                    <option value="ADMIN SURVEY">ADMIN SURVEY</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-5">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-5">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-5 offset-md-4 ">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>

    </div>
    <script type="text/javascript">
        jQuery(document).ready(function ()
        {
                jQuery('select[name="kodearea"]').on('change',function(){
                var areaID = jQuery(this).val();
                if(areaID)
                {
                    jQuery.ajax({
                        url : '/user/gettpk/' +areaID,
                        type : "GET",
                        dataType : "json",
                        success:function(data)
                        {
                            console.log(data);
                            jQuery('select[name="kodetpk"]').empty();
                            jQuery.each(data, function(id, namatpk){
                            $('select[name="kodetpk"]').append('<option value="'+ id +'">'+ namatpk +'</option>');
                            });
                        }
                    });
                }
                else
                {
                    $('select[name="kodetpk"]').empty();
                }
                });
        });
        </script>
@endsection
