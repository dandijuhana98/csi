@extends('layouts.app')
@section('content')
 
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Daftar user</h3>
            </div>
        </div> 
        <br>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>        
        </div>
    @endif

    <div class="form-row">
        <div class="form-group mb-2 mr-auto">
            <a class="btn btn-success" href="/user/create"> Tambah Data </a>
        </div>
        <div class="form-group mb-2 ml-auto">
            <input class="form-control" id="myInput" type="text" placeholder="Search.."><br>
        </div>
    </div>
    
    <table class="table table-striped" id="table" data-toggle="table">
      <thead>
        <tr>
            <th data-order='desc' data-sortable="true">No</th>
            <th data-order='desc' data-sortable="true">User</th>
            <!-- <th data-order='desc' data-sortable="true">Action</th> -->
        </tr>
      </thead>
      <tbody id="myTable">
        @foreach ($user as $p) 
            <tr>
                <td>{{++$i}}</td>
                <td>{{$p->email}}</td>
                
            </tr>
        @endforeach
        </tbody>
    </table>

    <br>
    Halaman : {{ $user->currentPage() }} <br/>
	Jumlah Data : {{ $user->total() }} <br/>
	Data Per Halaman : {{ $user->perPage() }} <br/>

    {!! $user->links() !!}
    </div>
    <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
    </script>

@endsection