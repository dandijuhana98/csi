@extends('layouts.app')
@section('content')

    <div class="container">
        <div class=" form-row">
            <div class="col-lg-12">
                <h3>Edit Data User</h3>
            </div>
        </div>
        <br>

        @if ($errors->all())
            <div class="alert alert-danger">
                <strong>Whoops! </strong> Ada permasalahan inputanmu.<br>
                <ul>
                    @foreach ($errors as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="{{route('user.update',$user->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="name" class="form-control" value="{{$user->name}}" placeholder="">
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" name="name"  class="form-control" id="user" value="{{$user->name}}" placeholder="">
                </div>
            </div>
            <input type="hidden" name="email" class="form-control" value="{{$user->email}}" placeholder="">
            <div class="form-group row">
                <label for="user" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" name="email"  class="form-control" id="user" value="{{$user->email}}" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Area</label>
                            <div class="col-sm-6">
                                <select name="kodearea"class="form-control" required>
                                @foreach($area as $area)
                                    <option value="{{ $area->id }}">{{ $area->namaarea}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">TPK</label>
                            <div class="col-sm-6">
                                <select name="kodetpk"class="form-control" required>
                                    <option value="">-Pilih-</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Role</label>
                            <div class="col-sm-6">
                                <select name="role"class="form-control" required>
                                    <option value="ADMIN">ADMIN</option>
                                    <option value="ADMIN PUSAT">ADMIN PUSAT</option>
                                    <option value="ADMIN AREA">ADMIN AREA</option>
                                    <option value="ADMIN TPK">ADMIN TPK</option>
                                </select>
                            </div>
                        </div>
            <div class="form-group row">
                <label for="status" class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                    <select id="status" name="status"class="form-control">
                        <option  {{$user->status == 'Aktif' ? 'Selected' : ''}} value="Aktif">Aktif</option>
                        <option  {{$user->status == 'Non Aktif' ? 'Selected' : ''}} value="Non Aktif">Non Aktif</option>
                    </select>
                </div>
            </div>
             <hr>
                <div class="form-group">
                    <a href="{{route('user.index')}}" class="btn btn-success">Kembali</a>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
        </form>

    </div>
@endsection