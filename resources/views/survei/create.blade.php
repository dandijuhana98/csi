@extends('layouts.newapp1')
@section('content')

<div class="container">
        <div class=" form-row">
            <div class="col-lg-12 text-left">
                <!-- <h3 class="dua">{{$tpk->namatpk}}</h3>
                <p class="tiga" >Halo {{$survey->nama}}</p> -->
            </div>
        </div>
        <br>

        @if ($errors->all())
            <div class="alert alert-danger">
                <strong>Whoops! </strong> Ada permasalahan inputanmu.<br>
                <ul>
                    @foreach ($errors as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="{{route('survei.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            @foreach ($pertanyaan as $p) 
            <div  class="container text-center">
            <p class="dua">{{++$i}}. {{$p->pertanyaan}}</p>
                <input type="hidden" name="id_pertanyaan" value="{{$p->id}}">
                <input type="hidden" name="id_survei" value="{{$survey->id}}">
                <span class="star-rating star-5 satu">
                    <input class="form-check-input" type="radio" name="rating{{$p->id}}"  value="1"><i></i>
                    <input class="form-check-input" type="radio" name="rating{{$p->id}}"  value="2"><i></i>
                    <input class="form-check-input" type="radio" name="rating{{$p->id}}"  value="3"><i></i>
                    <input class="form-check-input" type="radio" name="rating{{$p->id}}"  value="4"><i></i>
                    <input class="form-check-input" type="radio" name="rating{{$p->id}}"  value="5"><i></i>
                </span>
            </div>
        
            @endforeach
            <hr>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
  
    </div>

@endsection