@extends('layouts.newapp')
@section('content')





    <section class="wellcome_area clearfix" id="home">
    <div class="container">
        <div class=" form-row">
            <div class="col-lg-12">
                <!-- <h3>Survey {{$tpk->namatpk}}</h3> -->
            </div>
        </div>
        <br>

        @if ($errors->all())
            <div class="alert alert-danger">
                <strong>Whoops! </strong> Ada permasalahan inputanmu.<br>
                <ul>
                    @foreach ($errors as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <form action="{{route('survei.create')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div  class="container text-center">
            <input type="hidden"  name="kodetpk" class="form-control" value="{{ Auth::user()->kodetpk }}">
            <div class="form-group row">
                <label for="nikes" class="col-sm-2 col-form-label">Nikes</label>
                <div class="col-sm-4">
                    <input type="text"  name="nikes" class="form-control" id="nikes" placeholder="">
                </div>
            </div>
            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-4">
                    <input type="text"  name="nama" class="form-control" id="nama" placeholder="" >
                </div>
            </div>
            <div class="form-group row">
                <label for="telepon" class="col-sm-2 col-form-label">Telepon</label>
                <div class="col-sm-4">
                    <input type="number" name="telepon" class="form-control" id="telepon" placeholder="" >
                </div>
            </div>
            
             <hr>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Isi Survei</button>
                </div>
                </div>
        </form>

    </div>
    </section>
@endsection