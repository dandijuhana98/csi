<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailSurvei extends Model
{
    //
    protected $table = "detailsurvei";

    protected $fillable = ['id_survei', 'id_pertanyaan', 'rating'];
}
