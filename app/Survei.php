<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survei extends Model
{
    protected $table = "survei";

    protected $fillable = ['nikes', 'nama', 'telepon', 'kodetpk'];
}
