<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function index(){

        $user = User::orderBy('id','asc')->paginate(50);
        return view('user.index', compact('user'))
                ->with('i',(request()->input('page',1) -1)*10);
    }

    public function create(Request $request)
    {
        $area = DB::table('area')->select('id','namaarea')->get();
        return view('user.create', compact('area'));
    }

    public function getTpk($id) 
    {
        $tpk = DB::table("tpk")->where("kodearea", $id)->pluck("namatpk","id");
        return json_encode($tpk);
    }

    protected function store(Request $data)
    {
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'kodearea' => $data['kodearea'],
            'kodetpk' => $data['kodetpk'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
        ]);
        
        return redirect()->route('user.index')
                ->with('success','Data berhasil ditambahkan');
    }
}
