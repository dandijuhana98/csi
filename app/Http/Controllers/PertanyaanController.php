<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tpk = DB::table('tpk')->where('id', '=', Auth::user()->kodetpk)->first();
        $pertanyaan = Pertanyaan::where('kodetpk', Auth::user()->kodetpk)
                                ->paginate(10);
        return view('pertanyaan.index',compact('pertanyaan', 'tpk'))
                ->with('i',(request()->input('page',1) -1)*10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'kodetpk' => 'required',
            'status' => 'required',
        ]);
        
        Pertanyaan::create($request->all());
        return redirect()->route('pertanyaan.index')
                         ->with('success','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required',
            'kodetpk' => 'required',
            'status' => 'required',
        ]);
        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->pertanyaan = $request->get('pertanyaan');
        $pertanyaan->kodetpk = $request->get('kodetpk');
        $pertanyaan->status = $request->get('status');
        $pertanyaan->save();
        return redirect()->route('pertanyaan.index')
                         ->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->delete();
        return redirect()->route('pertanyaan.index')
                         ->with('success', 'Data berhasil dihapus');
    }
}
