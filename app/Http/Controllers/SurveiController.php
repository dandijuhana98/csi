<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Pertanyaan;
use App\Survei;
use App\DetailSurvei;

class SurveiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tpk = DB::table('tpk')->where('id', '=', Auth::user()->kodetpk)->first();
        return view('survei.index', compact('tpk'));
    }

    public function formSurvei(Request $request){
        Survei::create($request->all());

        $survey = DB::table('survei')->latest('id')->first();
        $tpk = DB::table('tpk')->where('id', '=', Auth::user()->kodetpk)->first();
        $pertanyaan = Pertanyaan::where('kodetpk', Auth::user()->kodetpk)
                                ->where('status', '=', 'Aktif')
                                ->paginate(10);
        return view('survei.create', compact('pertanyaan', 'tpk', 'survey'))
                ->with('i',(request()->input('page',1) -1)*10);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
               // $rating = new Rating;
    // $rating->rating = 4;
    // $rating->user_id = \Auth::id();
    // $food->ratings()->save($rating);
    $survei = new Survei;
    $survei->id_survei = \Auth::id();
    $survei->survei = $request->input('star');
    $pertanyaan = Pertanyaan::find($request->input('id'));
    $survei->pertanyaan()->save($survei);
    return response()->json(['survei' => $survey->survei]);
        
        return view('survei.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
